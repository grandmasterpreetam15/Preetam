package p1;

import java.util.Scanner;

public class Day1Demo6 {

	public static void main(String[] args) {

		System.out.println("Enter a decimal");
		int n = Read.sc.nextInt();
		System.out.println(Integer.toBinaryString(n));
		System.out.println(Integer.toOctalString(n));
		System.out.println(Integer.toHexString(n));
		
	}

}
