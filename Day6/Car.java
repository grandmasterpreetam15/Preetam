package inheritance;


public class Car extends Vehicle {

	String carType;
	
	
	public Car() 
	{
		System.out.println("\nCar Constructor.");	
	}
	
	
	public void start()
	{
		System.out.println("\nCar Started.");

	}
	
	public void showDetails() 
	{
		color="Blue";
		noOfWheels=4;
		carType="Hatch Back";

		System.out.println("Car Color is:"+color);
		System.out.println("No of Wheels Car has is:"+noOfWheels);
		System.out.println("Car Type is:"+carType);
		
	}

	@Override
	public void stop() {
		System.out.println("Stopped.");		
	}
	
}
