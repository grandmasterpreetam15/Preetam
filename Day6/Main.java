package inheritance;

public class Main {

	public static void main(String[] args) {

		Car c=new Car();
		c.start();
		c.showDetails();
		
		Bus b=new Bus();
		b.start();
		b.showDetails();
		
		Honda h=new Honda();
		h.start();
		h.kickstart();
		h.stop();
		
		Vehicle vehi=new Car();
		vehi.start();
		vehi.stop();
		
		vehi=new Honda();
		vehi.start();
		vehi.stop();
		
	}

}
