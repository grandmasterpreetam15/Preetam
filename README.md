1)whats DBMS?
	A database is an organized collection of data, stored and accessed electronically. 
Database designers typically organize the data to model aspects of reality in a way that 
supports processes requiring information.A database management system (DBMS) is a software 
application that interacts with end users, other applications, 
and the database itself to capture and analyze data. A general-purpose 
DBMS allows the definition, creation, querying, update, and administration of databases.


2)whats normalization?
	Normalization is the process of reorganizing data in a database so that it meets two basic requirements: 
(1) There is no redundancy of data (all data is stored in only one place), 
(2) data dependencies are logical (all related data items are stored together).
its of 3types
1NF
2NF
3NF


3)whats file-decomposition?
	its a process of dividing the file into various file so as to obtain the values that are needed for us.


4)whats table spaces?
	A tablespace is a logical group of data files in a database. A database typically contains at least one tablespace, and usually two or more. 
Within the database, the tablespace plays a role similar to that of a folder on the hard drive of a computer.


