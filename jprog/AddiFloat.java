import java.text.DecimalFormat;
public class AddiFloat
{
public static void main(String[] arg)
{
if(arg.length!=2)
{
System.out.println("Arguement mismatched plz enter 2 no.s only");
return;
}
DecimalFormat df = new DecimalFormat("#,###,##0.00");
float a=Float.parseFloat(arg[0]);
float b=Float.parseFloat(arg[1]);
float c=a+b;
System.out.println(" a = "+df.format(a) + " b =" + b +" sum =" +df.format(c));
}
}