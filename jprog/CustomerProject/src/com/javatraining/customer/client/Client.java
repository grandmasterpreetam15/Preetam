package com.javatraining.customer.client;

import java.util.ArrayList;
import java.util.List;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {

		Customer customer=new Customer(5,"shruthi","Bangalore",56000);
		
		CustomerDAO customerDAO=new CustomerDAOImpl();
		
		//testing the method for insert
//		int result=customerDAO.insertCustomer(customer);
//		System.out.println(result+" rows affected.");
		
		
		
		//testing the method for update
//		int customerId=5;
//		String newCustomerAddress="Agra";
//		int newBillAmount=5900;
//		int result=customerDAO.updateCustomer(customerId, newCustomerAddress, newBillAmount);
//		System.out.println(result+" rows updated.");
		
		
		//testing the listall Method
		List<Customer> customers=new ArrayList<Customer>();
		customers=customerDAO.listAllCustomers();
		System.out.println(customers);
		
		
		//testing delete customer
//		int customerId=5;
//		CustomerDAO dao=new CustomerDAOImpl();
//		int rows=dao.deleteCustomer(customerId);
//		System.out.println(rows+" deleted.");
		
		
		
//		//Testing customer exists
//		CustomerDAO dao=new CustomerDAOImpl();
//		if (dao.isCustomerExists(3))
//		{
//			System.out.println("Exixts");
//			System.out.println(dao.findByCustomerId(3));
//		}
//		else
//		{
//			System.out.println("Does not exist.");
//		}
//		
	}

}
