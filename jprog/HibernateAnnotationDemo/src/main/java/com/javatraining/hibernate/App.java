package com.javatraining.hibernate;

import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws ClassNotFoundException, SQLException
    {
    	Customer customer = new Customer(1997, "Amya", "Hyderabad", 5000);
//    	Configuration configuration = new Configuration().configure();
    	
    	AnnotationConfiguration configuration = new AnnotationConfiguration().configure();
    	SessionFactory factory=configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	Transaction transaction=session.beginTransaction();
    	session.save(customer);
    	transaction.commit();
    	System.out.println("Data Stored");
    	session.close();
    	factory.close();
//    	Class.forName("oracle.jdbc.driver.OracleDriver");
//    	Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:orcl","scott","tiger");
//        System.out.println( "Connected" );
    }
}
