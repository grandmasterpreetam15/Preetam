package com.javatraining.hibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class AppDelete 
{
    public static void main( String[] args ) throws ClassNotFoundException, SQLException
    {
    	Customer customer=new Customer();
    	customer.setCustomerId(1991);
    	Configuration configuration = new Configuration().configure();
    	SessionFactory factory=configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	Transaction transaction=session.beginTransaction();
    	session.delete(customer);
    	transaction.commit();
    	System.out.println("Customer Deleted");
    	session.close();
    	factory.close();
//    	Class.forName("oracle.jdbc.driver.OracleDriver");
//    	Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:orcl","scott","tiger");
//        System.out.println( "Connected" );
    }
}
