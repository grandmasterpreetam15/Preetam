package com.javatraining.hibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class AppSelectAll 
{
    public static void main( String[] args ) throws ClassNotFoundException, SQLException
    {
    	Customer customer=new Customer();
   
    	Configuration configuration = new Configuration().configure();
    	SessionFactory factory=configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	System.out.println("All Customer details");
    	
//    	Query q= session.createQuery("from Customer where customerAddress like 'Hyderabad'");
//    	Query q= session.createQuery("from Customer where customerId=1992");
    	
    	Criteria q=session.createCriteria(Customer.class).add(Restrictions.eq("customerId", 1994)).add(Restrictions.gt("billAmount", 4000));
    	List<Customer> customers=q.list();
    	
    	Iterator<Customer> iterator=customers.iterator();
    	
    	while(iterator.hasNext())
    	{
    		Customer cust = iterator.next();
    		System.out.println(cust);
    	}
    	
    	session.close();
    	factory.close();
//    	Class.forName("oracle.jdbc.driver.OracleDriver");
//    	Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:orcl","scott","tiger");
//        System.out.println( "Connected" );
    }
}
