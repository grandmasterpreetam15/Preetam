package com.javatraining.hibernate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.customer.model.Customer;

/**
 * Hello world!
 *
 */
public class AppGetTheData 
{
    public static void main( String[] args ) throws ClassNotFoundException, SQLException
    {
    	Customer customer=new Customer();
//    	Customer customer = new Customer(1990, "Preetam", "Hyderabad", 5000);
    	Configuration configuration = new Configuration().configure();
    	SessionFactory factory=configuration.buildSessionFactory();
    	
    	Session session=factory.openSession();
    	Transaction transaction=session.beginTransaction();
    	customer=( Customer) session.get(Customer.class, 1993);
    	customer.setCustomeNmae("Kavya");
    	System.out.println(customer);
//    	session.save(customer);
    	transaction.commit();
    	System.out.println("Details update");
    	session.close();
    	factory.close();
//    	Class.forName("oracle.jdbc.driver.OracleDriver");
//    	Connection connection=DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:orcl","scott","tiger");
//        System.out.println( "Connected" );
    }
}
