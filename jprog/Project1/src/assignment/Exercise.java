package assignment;

public class Exercise {
	String str="The quick brown fox jumps over the lazy dog";
	
	public void display()
	{
		System.out.println(str.charAt(12));
		System.out.println(str.contains("is"));
		System.out.println(str.concat("and killed it"));
		System.out.println(str.endsWith("dogs"));
		System.out.println(str.equals("The quick brown Fox jumps over the lazy Dog"));
		System.out.println(str.equals("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"));
		System.out.println(str.length());
		System.out.println(str.matches("The quick brown Fox jumps over the lazy Dog"));
		System.out.println(str.replaceAll("The", "A"));
		System.out.println(str.split("Fox"));
		System.out.println();
	}
	public static void main(String[] args) {
		Exercise n= new Exercise();
		n.display();
	}

}
