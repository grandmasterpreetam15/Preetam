package collectiondemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
public class Demo4 {

	public static void main(String[] args) {

		Customer cust1=new Customer(1,"Mohan","Pune",9800);
		Customer cust2=new Customer(2,"Anu","Mumbai",800);
		Customer cust3=new Customer(3,"Zeba","Agra",1800);
		Customer cust4=new Customer(4,"Uday","Jaipur",2900);
		Customer cust5=new Customer(5,"Neeti","Delhi",200);
		
		List<Customer> allCustomers=new ArrayList<Customer>();
		allCustomers.add(cust1);
		allCustomers.add(cust2);
		allCustomers.add(cust3);
		allCustomers.add(cust4);
		allCustomers.add(cust5);
		
		Iterator<Customer> i= allCustomers.iterator();
		
		while(i.hasNext())
		{
			System.out.println(i.next());
		}
		
		
		Collections.sort(allCustomers,new AddressComparator());
		System.out.println("After Sorting on Address");
Iterator<Customer> k= allCustomers.iterator();
		
		while(k.hasNext())
		{
			System.out.println(k.next());
		}
		Collections.sort(allCustomers,new BillAmountComparator());
		System.out.println("After Sorting on Bill Amount");
		Iterator<Customer> j= allCustomers.iterator();
		while(j.hasNext())
		{
			System.out.println(j.next());
		}
		
		Collections.sort(allCustomers,new Comparator<Customer>() {
			public int compare(Customer o1,Customer o2) {
				if(o1.getCustomerId()< o2.getCustomerId())
					return 1;
				else
					return -1;
			}
			
		});
	}

}
