package collectiondemo;

public class Generic<T> {
//	public <T extends Number> double disp(T one,T two)
//	{
//		
//		return one.doubleValue() + two.doubleValue();
//	}
//public static void main(String[] args) {
//	Generic<Integer> d=new Generic<Integer>();
//	System.out.println(d.disp(15,16));
//}
//}
	
	
	public <T extends Number> int disp(T one,T two)
	{
		
		return one.intValue() + two.intValue();
	}
public static void main(String[] args) {
	Generic<Integer> d=new Generic<Integer>();
	System.out.println(d.disp(15,16));
}
}

	