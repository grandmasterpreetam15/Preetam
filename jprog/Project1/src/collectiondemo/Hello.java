package collectiondemo;

public class Hello<Z,Y> {
	public void display(Z s,Y y) {
		System.out.println(s+" "+y);
		
	}
public static void main(String[] args) {
	Hello<Boolean,String> hello = new Hello<Boolean,String>();
	hello.display(true,"Kavya");

	Hello<String,Integer> hello1 = new Hello<String,Integer>();
	hello1.display("Preetam",2);
}

}
