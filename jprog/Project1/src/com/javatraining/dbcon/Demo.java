package com.javatraining.dbcon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class Demo {
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the customerId:");
		int customerId = sc.nextInt();
		Connection connection=DBConfig.getConnection();
		System.out.println("Enter customer address");
		String customerAdd = sc.next();
		System.out.println("Enter customer billamount");
		int billAmount = sc.nextInt();
		PreparedStatement statement = connection.prepareStatement("update customer set customerAddress=?, billAmount=? where customerId="+customerId);
		statement.setString(1, customerAdd);
		statement.setInt(2, billAmount);
		
int rows = statement.executeUpdate();
		
		System.out.println(rows + "Updated");
		

	}

}

	
