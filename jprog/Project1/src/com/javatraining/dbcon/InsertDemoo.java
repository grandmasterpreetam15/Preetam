package com.javatraining.dbcon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


public class InsertDemoo {
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the customerId");
		int customerId = sc.nextInt();
		System.out.println("Enter customer name");
		String customerName = sc.next();
		System.out.println("Enter customer address");
		String customerAdd = sc.next();
		System.out.println("Enter customer billamount");
		int billAmount = sc.nextInt();
		Connection connection=DBConfig.getConnection();
		PreparedStatement statement = connection.prepareStatement("insert into customer values(?,?,?,?)");
		statement.setInt(1, customerId);
		statement.setString(2, customerName);
		statement.setString(3, customerAdd);
		statement.setInt(4, billAmount);
		
int rows = statement.executeUpdate();
		
		System.out.println(rows + "Updated");
		

	}

}
	
