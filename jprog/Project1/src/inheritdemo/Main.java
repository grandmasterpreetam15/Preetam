package inheritdemo;

class Vehicle {
	String color;
	int noOfWheels;
	void start() {
		System.out.println("VEhicle started");
	}

}
class Car extends Vehicle {
	String carType;
	String color="white";
	
	protected void start() {
		System.out.println("car Started");
	}
	public void showDetails()
	{
		color="red";
		noOfWheels=4;
		carType="hatchback";
		super.start();
		System.out.println("Car color=" +color);
		System.out.println("Car has no of wheels=" +noOfWheels);
		System.out.println("Car type=" +carType);
	}

}

public class Main {

	public static void main(String[] args) {
	
		Car c=new Car();
		c.showDetails();
		c.start();
		

	}

}
