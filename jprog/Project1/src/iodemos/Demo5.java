package iodemos;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class Demo5 {

	
	public static void main(String[] args) throws IOException {
		BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(new File("c:\\training\\SAP\\hello.txt")));
		int i=0;
		while((i=inputStream.read())!=-1)
		{
			System.out.print((char)i);
		}
		inputStream.close();
	}
}
