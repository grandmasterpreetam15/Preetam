package iodemos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Demo6 {

	public static void main(String[] args) throws  IOException {
		BufferedReader bread = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Please enter  your name:");
		String name = bread.readLine();
		
		System.out.println("Please enter your marks:");
		int marks = Integer.parseInt(bread.readLine());
		
		System.out.println(name +" scored "+marks);
	}

}
