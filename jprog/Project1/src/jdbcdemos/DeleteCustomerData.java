package jdbcdemos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.javatraining.dbcon.DBConfig;

public class DeleteCustomerData {

	public static void main(String[] args) throws SQLException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter user name");
		String username=sc.next();
		System.out.println("enter password");
		String password=sc.next();
		
		Connection conn=DBConfig.getConnection();
		PreparedStatement statement=conn.prepareStatement("select username,password from client where username=? and password=?");
		statement.setString(1, username);
		statement.setString(2, password);
		ResultSet results=statement.executeQuery();
		if(results.next())
		{
			System.out.println("username exists");
		}
		else
		{
			System.out.println("username doesnt exists");
		}
	}
	
	
	
}
