package jdbcdemos;

import java.sql.Statement;

import com.javatraining.dbcon.DBConfig;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Demo1 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Connection connection=DBConfig.getConnection();
	Statement statement=(Statement) connection.createStatement();	
	ResultSet res= statement.executeQuery("select * from customer");
	
	while(res.next())
	{
		System.out.print(res.getString(1) + " ");
		System.out.print(res.getString(2)+ " ");
		System.out.print(res.getString(3)+ " ");
		System.out.println(res.getString(4));
	}
		
	res.close();
	 statement.close();
	 connection.close();
	
	}

}