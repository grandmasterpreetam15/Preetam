package p3;

public class Main3 {
 public static void main(String[] args) {
	Customer customer1 = new Customer(112,"Mohan","Pune",12600);
	Customer customer2 = new Customer(112,"Mohan","Pune",12600);
	customer1.setCustomerAddress("Mumbai");
	customer2.setCustomerAddress("Mumbai");
	
	System.out.println();
	
	System.out.println(customer1);
	System.out.println(customer2);
	System.out.println();
	
	System.out.println("customer1.equals(customer2):" +customer1.equals(customer2));
	System.out.println();
	
	System.out.println("hash code value of customer1:" +customer1.hashCode());
	System.out.println("hash code value of customer2:" +customer2.hashCode());
}
	
}

