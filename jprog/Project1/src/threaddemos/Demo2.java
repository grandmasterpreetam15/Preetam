package threaddemos;

public class Demo2 extends Thread {
	
	
//	public void display(String threadName,String message1,String message2)
//	{
//		System.out.println("Message by:"+threadName);
//		System.out.println("Message 1 is:"+message1);
//		try {
//			Thread.sleep(1000);
//		}catch(InterruptedException e) {
//			e.printStackTrace();
//		}
//		System.out.println("Message 2 is:"+message2);
//	}
//	
//	public Demo2(int i) {
//		super("ITPL"+i);
//		start();
//	}
//	@Override
//	public void run() {
//		display(Thread.currentThread().getName(), "Hi", "bye");
//		System.out.println("RUN CALLED BY:"+Thread.currentThread().getName());
//	}
//	public static void main(String[] args) {
//		for(int i=1;i<=3;i++)
//			new Demo2(i);
//		System.out.println("MAIN CALLED BY:"+Thread.currentThread().getName());
//	}
//
//}

	
	
	public static synchronized void display(String threadName,String message1,String message2)
	{
		System.out.println("Message by:"+threadName);
		System.out.println("Message 1 is:"+message1);
		try {
			Thread.sleep(1000);
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Message 2 is:"+message2);
	}
	
	public Demo2(int i) {
		super(""+i);
		start();
	}
	@Override
	public void run() {
		display(Thread.currentThread().getName(), "Hi", "bye");
		
	}
	public static void main(String[] args) {
		for(int i=1;i<=3;i++)
			new Demo2(i);
		
	}

}
