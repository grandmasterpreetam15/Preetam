package threaddemos;

public class Demo3 extends Thread {
	
//	public Demo3(String n) {
//		super(n);
//		start();
//	}
//	@Override
//	public void run() {
//		System.out.println("RUN CALLED BY:"+Thread.currentThread().getName());
//	}
//	public static void main(String[] args) {
//		new Demo3("ITPL1");
//		new Demo3("ITPL2");
//		new Demo3("ITPL3");
//		System.out.println("MAIN CALLED BY:"+Thread.currentThread().getName());
//	}
//
//}

	
	public Demo3(int i) {
		super("ITPL"+i);
		start();
	}
	@Override
	public void run() {
		System.out.println("RUN CALLED BY:"+Thread.currentThread().getName());
	}
	public static void main(String[] args) {
		for(int i=1;i<=5;i++)
			new Demo3(i);
		System.out.println("MAIN CALLED BY:"+Thread.currentThread().getName());
	}

}