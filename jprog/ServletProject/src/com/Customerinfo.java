package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class Customerinfo
 */
public class Customerinfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Customerinfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("customerId"));
		String name=request.getParameter("customerName");
		String add=request.getParameter("customerAddress");
		int bamt=Integer.parseInt(request.getParameter("billAmount"));
		
		response.getWriter().println(" <br/>User id is: " +id );
		response.getWriter().println(" <br/>User name is: " +name );
		response.getWriter().println(" <br/>User address is: " +add );
		response.getWriter().println(" <br/>User billamount is: " +bamt );

		Customer customer=new Customer(id,name,add,bamt);
		
		CustomerDAO customerDAO=new CustomerDAOImpl();
		
		//testing the method for insert
		int result=customerDAO.insertCustomer(customer);
		response.getWriter().println(result+" rows affected.");
		
		
	}

}
