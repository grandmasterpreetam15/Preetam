package com.javatraining.customer.client;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.customer.model.Customer;

public class client {

	public static void main(String[] args) {
//		Customer customer=new Customer();
//		customer.setCustomeNmae("Preetam");
//		System.out.println("Customer name is:"+customer.getCustomeNmae());

		
		Resource resource=new ClassPathResource("beans.xml");
		BeanFactory factory=new XmlBeanFactory(resource);
		
		
		Customer customer=(Customer)factory.getBean("cust");
//		customer.setCustomeNmae("Preetam");
		
		System.out.println(customer);
	}

}
