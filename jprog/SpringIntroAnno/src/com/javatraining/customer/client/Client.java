package com.javatraining.customer.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.javatraining.config.AppConfig;
import com.javatraining.customer.model.BankAccount;
import com.javatraining.customer.model.Customer;

public class Client {

	public static void main(String[] args) {
		
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		
		Customer customer1 = context.getBean(Customer.class);
		customer1.setCustomeNmae("Preetam");
		customer1.setCustomerId(100);
		customer1.setCustomerAddress("Hyderabad");
		customer1.setBillAmount(1230);
		BankAccount bankAccount=context.getBean(BankAccount.class);
		bankAccount.setAccountNumber("0274");
		bankAccount.setBalance(2588);
		
//		customer1.setBankAccount(bankAccount);
		System.out.println(customer1);

	}

}
