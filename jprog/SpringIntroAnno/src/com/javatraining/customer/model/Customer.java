package com.javatraining.customer.model;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

public class Customer {
	
	private int customerId;
	private String customeNmae;
	private String customerAddress;
	private int billAmount;
	
	@Autowired
	private BankAccount bankAccount;

	@PostConstruct
	public void preetam() {
		System.out.println("I am Preetam");
	}
	
	@PostConstruct
	public void kavya() {
		System.out.println("I am Kavya");
	}

	public Customer() {
		System.out.println("In default constructor");
	}

	public Customer(int customerId, String customeNmae, String customerAddress, int billAmount) {
		super();
		this.customerId = customerId;
		this.customeNmae = customeNmae;
		this.customerAddress = customerAddress;
		this.billAmount = billAmount;
		System.out.println("in paramaterised constructor");
	}

	public Customer(int customerId, String customeNmae, String customerAddress, int billAmount,
			BankAccount bankAccount) {
		super();
		this.customerId = customerId;
		this.customeNmae = customeNmae;
		this.customerAddress = customerAddress;
		this.billAmount = billAmount;
		this.bankAccount = bankAccount;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomeNmae() {
		return customeNmae;
	}

	public void setCustomeNmae(String customeNmae) {
		this.customeNmae = customeNmae;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public int getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(int billAmount) {
		this.billAmount = billAmount;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customeNmae=" + customeNmae + ", customerAddress="
				+ customerAddress + ", billAmount=" + billAmount + ", bankAccount=" + bankAccount + "]";
	}

	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + billAmount;
		result = prime * result + ((customeNmae == null) ? 0 : customeNmae.hashCode());
		result = prime * result + ((customerAddress == null) ? 0 : customerAddress.hashCode());
		result = prime * result + customerId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (billAmount != other.billAmount)
			return false;
		if (customeNmae == null) {
			if (other.customeNmae != null)
				return false;
		} else if (!customeNmae.equals(other.customeNmae))
			return false;
		if (customerAddress == null) {
			if (other.customerAddress != null)
				return false;
		} else if (!customerAddress.equals(other.customerAddress))
			return false;
		if (customerId != other.customerId)
			return false;
		return true;
	}

	

//	@Override
//	public int compareTo(Customer o) {
//
//		if(this.billAmount<o.billAmount)
//			return 1;
//		else
//			return -1;
//	}
	
	

}


