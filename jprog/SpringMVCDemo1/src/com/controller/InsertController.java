package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Guest;

@Controller
public class InsertController {
	
	@RequestMapping("/insert")
	public ModelAndView getA()
	{
		ModelAndView view=new ModelAndView();
		view.setViewName("in");
		view.addObject("message","Good Morning from controller");
		return view;
	}
	
	@RequestMapping("/guest")
	public ModelAndView getGuestView(Guest guest)
	{
		ModelAndView view=new ModelAndView();
		view.setViewName("guestDetails");
		view.addObject("message","Good Morning");
		view.addObject("guestInfo", guest);
		return view;
	}


}
