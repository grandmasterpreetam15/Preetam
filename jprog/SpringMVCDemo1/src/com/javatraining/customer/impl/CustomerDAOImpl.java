package com.javatraining.customer.impl;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.dbcon.DBConfig;
import com.javatraining.customer.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {

	@Override
	public int insertCustomer(Customer customer) {
		Connection conn = DBConfig.getConnection();
		int rows=0;
		
		try 
		{
			PreparedStatement statement = conn.prepareStatement("insert into customer values(?,?,?,?)");
			statement.setInt(1, customer.getCustomerId());
			statement.setString(2, customer.getCustomerName());
			statement.setString(3, customer.getCustomerAddress());
			statement.setInt(4, customer.getBillAmount());
			
			rows = statement.executeUpdate();
		//	System.out.println(rows+" Updated.");
			conn.close();
		} 
		
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return rows;
	}

	@Override
	public int updateCustomer(int customerId, String newCustomerAddress, int newBillAmount) {
		int rows = 0;
		try {
			Connection conn = DBConfig.getConnection();
			PreparedStatement statement = conn.prepareStatement("update customer set customerAddress=?, billAmount=? where customerId=?");
			statement.setString(1, newCustomerAddress);
			statement.setInt(2, newBillAmount);
			statement.setInt(3, customerId);
			
			
			rows = statement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rows;
	}

	@Override
	public int deleteCustomer(int customerId) {
		Connection connection=DBConfig.getConnection();
		int row=0;
		String query=" delete from customer where customerId=?";
		try
		{
			PreparedStatement statement=connection.prepareStatement(query);
			row=statement.executeUpdate();
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
			
		return row;
	}

	@Override
	public Customer findByCustomerId(int customerId) {
		Connection connection = DBConfig.getConnection();
        Customer customer = new Customer();
        ResultSet resultSet = null;
        String selectQuery = "select * from customer where customerId = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(selectQuery);
            statement.setInt(1, customerId);
            resultSet = statement.executeQuery();
            resultSet.next();
            customer.setCustomerId(resultSet.getInt(1));
            customer.setCustomerName(resultSet.getString(2));
            customer.setCustomerAddress(resultSet.getString(3));
            customer.setBillAmount(resultSet.getInt(4));
           
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }  
        return customer;
		
	}

	@Override
	public boolean isCustomerExists(int customerId) {
		Connection conn=DBConfig.getConnection();
		ResultSet res=null;
		boolean result=false;
		
		String query=" select * from customer where customerId=?";
		try
		{
			PreparedStatement statement=conn.prepareStatement(query);
			statement.setInt(1, customerId);
			res=statement.executeQuery();
			result=res.next();
			conn.close();

		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
			
		return result;
	}

	@Override
	public List<Customer> listAllCustomers() {
		List<Customer> customers = new ArrayList<Customer>();
		Connection conn=DBConfig.getConnection();
		String query="select * from customer";
		try {
			Statement statement = conn.createStatement();
			ResultSet resultSet=statement.executeQuery(query);
			while(resultSet.next())
			{
				Customer customer=new Customer();
				customer.setCustomerId(resultSet.getInt(1));
				customer.setCustomerName(resultSet.getString(2));
				customer.setCustomerAddress(resultSet.getString(3));
				customer.setBillAmount(resultSet.getInt(4));
				customers.add(customer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customers;
	}

	

	

}
